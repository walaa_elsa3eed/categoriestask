# PayTabs categories and subcategories task

## Info about project:

this is a task for PayTabs company prepared by [Walaa Elsaeed](https://eg.linkedin.com/in/walaa-elsaeed-4b9216bb)

## Setup
Copy `env` to `.env` and tailor for your app, specifically the baseURL
and any database settings.
`php spark migrate` then `php spark db:seed CategoriesSeeder`

When done, you can visit [categories]({base_url}/Categories)
`{base_url}/Categories`.