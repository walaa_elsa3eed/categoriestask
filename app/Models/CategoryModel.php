<?php namespace App\Models;

use CodeIgniter\Model;

class CategoryModel extends Model
{
    protected $table      = 'categories';
    protected $primaryKey = 'id';
    protected $returnType     = 'array';
    protected $allowedFields = ['name', 'parent_id'];


    public function list($parent_id = null)
    {
        $categories = self::where('parent_id', $parent_id)->findAll();
        return $categories;
    }

}