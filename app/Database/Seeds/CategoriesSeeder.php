<?php
namespace App\Database\Seeds;
use App\Models\CategoryModel;
use \CodeIgniter\I18n\Time;
use Faker\Factory;
use \CodeIgniter\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    public function run()
    {
        $faker = Factory::create('id_ID');
        $random_number_array = range(1, 1000);
        for ($i = 0; $i < 1000; $i++)
        {
            $data = [
                'name' => $faker->name,
                'parent_id' => $faker->optional(0.9, null)->randomElement($random_number_array),
                'created_at'  => Time::now(),
                'updated_at'  => Time::now()
            ];
            $this->db->table('categories')->insert($data);
        }
    }
}