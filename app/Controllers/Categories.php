<?php


namespace App\Controllers;
use App\Models\CategoryModel;

class Categories extends BaseController
{
    private $categories;

    public function __construct()
    {
        $this->categories = new CategoryModel;
    }
    public function index()
    {
        $categories = $this->categories->list();
        return view('categories', ['categories' => $categories]);
    }

    public function categoriesList($parent_id)
    {
        $categories = $this->categories->list($parent_id);
        return json_encode( $categories );
    }

}