<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Categories and sub categories view</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body class="py-5">

<div class="container">
    <div class="row">
        <div class="12">
            <h1 >List Of Parent Categories</h1>

            <h3 class="text-center">Selcet each level to can view the below level of categories</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div id="childs">
                <select onchange="LoadCategories($(this))" class="form-text">
                    <option >- Select -</option>
                    <?php foreach ($categories as $category):?>
                        <option value="<?= $category['id'] ?>"><?= $category['name'] ?></option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

<script>
    function LoadCategories($parent_id)
    {
        $.ajax({
            url: 'Categories/categoriesList/'+ $parent_id.val(),
            type: 'GET',
            dataType: 'json',
            success:function(response){
                var len = response.length;
                if ($parent_id.next().length > 0)
                {
                    $parent_id.nextAll('select').remove();
                }
                if (len > 0)
                {
                    //$parent_id.next().remove();
                   var identifier = response[0]['parent_id'];
                    $("#childs").append("<select class=\"form-text\" id='childs-"+identifier+"'" +
                        "onchange='LoadCategories($(this))'><option >- Select -</option></select>")
                    for( var i = 0; i<len; i++)
                    {
                        var id = response[i]['id'];
                        var name = response[i]['name'];

                        $("#childs select").last().append("<option value='"+id+"'>"+name+"</option>");

                    }
                }
            }
        });
    }
</script>



</body>
</html>